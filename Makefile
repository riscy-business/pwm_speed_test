TARGET = pwm_speed_test
CFLAGS += -O2 -fno-builtin-printf -DUSE_PLIC
BSP_BASE = ../../bsp
C_SRCS += pwm_speed_test.c
C_SRCS += $(BSP_BASE)/drivers/plic/plic_driver.c
include $(BSP_BASE)/env/common.mk
