#include <stdio.h>
#include <stdlib.h>
#include "plic/plic_driver.h"
#include "encoding.h"
#include "platform.h"
plic_instance_t g_plic;
typedef void (*interrupt_function_ptr_t) (void);
interrupt_function_ptr_t g_ext_interrupt_handlers[PLIC_NUM_INTERRUPTS];
void pwm_0_handler()
{
    printf("PWM interrupt!\n\r");
    /* PWM0_REG(PWM_CFG)   = 0; */
    PWM0_REG(PWM_CFG) = ((PWM_CFG_ONESHOT) | (PWM_CFG_ZEROCMP) | 0x7 | (PWM_CFG_STICKY));
}
void handle_m_ext_interrupt()
{
    plic_source int_num  = PLIC_claim_interrupt(&g_plic);
    if ((int_num >=1 ) && (int_num < PLIC_NUM_INTERRUPTS))
        g_ext_interrupt_handlers[int_num]();
    else
        exit(1 + (uintptr_t) int_num);
    PLIC_complete_interrupt(&g_plic, int_num);
}
int main (void)
{
    PLIC_init(&g_plic,
            PLIC_CTRL_ADDR,
            PLIC_NUM_INTERRUPTS,
            PLIC_NUM_PRIORITIES);
    g_ext_interrupt_handlers[INT_PWM0_BASE] = pwm_0_handler;
    PLIC_enable_interrupt(&g_plic, INT_PWM0_BASE);
    PWM0_REG(PWM_CFG)   = 0;
    PWM0_REG(PWM_CFG) = ((PWM_CFG_ONESHOT) | (PWM_CFG_ZEROCMP) | 0x7 | (PWM_CFG_STICKY));
    /* PWM0_REG(PWM_CFG) = PWM_CFG_ENALWAYS | PWM_CFG_CMP0IP; */
    /* PWM0_REG(PWM_CFG) = ((PWM_CFG_ONESHOT) | (PWM_CFG_ZEROCMP)| 0x7 | (PWM_CFG_STICKY)); */

    /* PWM0_REG(PWM_COUNT) = 0; */
    /* PWM0_REG(PWM_CMP0)  = 0xFE; */
    /* PWM0_REG(PWM_CMP1)  = 0xFF; */
    /* PWM0_REG(PWM_CMP2)  = 0xFF; */
    /* PWM0_REG(PWM_CMP3)  = 0xFF; */
    PLIC_set_priority(&g_plic, INT_PWM0_BASE, 5);
    /* PWM0_REG(PWM_COUNT) = 0; */
    PWM0_REG(PWM_CMP0)  = 0xFFFF;
    set_csr(mie, MIP_MEIP);
    set_csr(mstatus, MSTATUS_MIE);
    printf("Hello, World!\n\r");
    volatile int i = 1;
    while (i)
    {
        asm volatile ("wfi");
    }
}
